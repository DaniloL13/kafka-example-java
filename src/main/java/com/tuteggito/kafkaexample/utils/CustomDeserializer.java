package com.tuteggito.kafkaexample.utils;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuteggito.kafkaexample.entities.EmailData;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.kafka.common.serialization.Deserializer;
import org.springframework.util.ObjectUtils;

import java.io.IOException;

@CommonsLog
public class CustomDeserializer implements Deserializer<EmailData> {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public EmailData deserialize(String s, byte[] bytes) {
        try {
            if(ObjectUtils.isEmpty(bytes)) {
                return null;
            }
            return objectMapper.readValue(bytes, EmailData.class);
        } catch (StreamReadException e) {
            log.error("An error occurred while trying to read stream", e);
        } catch (DatabindException e) {
            log.error("An error occurred with databind", e);
        } catch (IOException e) {
            log.error("An I/O exception has occurred", e);
        }
        return null;
    }
}
