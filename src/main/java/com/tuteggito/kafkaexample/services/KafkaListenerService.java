package com.tuteggito.kafkaexample.services;

import com.tuteggito.kafkaexample.entities.EmailData;
import com.tuteggito.kafkaexample.properties.KafkaProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class KafkaListenerService {
    private final KafkaProperties kafkaProperties;
    @KafkaListener(topics = "test-topic", groupId = "test-group")
    public void listener(List<EmailData> data) {
        data
                .parallelStream()
                .map(emailData -> {
                    return "Sending email to ["
                            .concat(emailData.getEmail())
                            .concat("] with subject [")
                            .concat(emailData.getSubject())
                            .concat("]");
                })
                .forEach(System.out::println);

    }
}
