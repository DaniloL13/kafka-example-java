package com.tuteggito.kafkaexample.properties;

import com.tuteggito.kafkaexample.utils.CustomDeserializer;
import lombok.Data;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@Data
@ConfigurationProperties(prefix = "app.kafka")
public class KafkaProperties {
    private String bootstrapServers;
    private String applicationId;
    private String groupId;
    private String inputTopic;
    private String kafkaDirectory;
    private Integer maxPollRecords;
    private Integer maxPollIntervalMs;

    public Map<String, Object> getProperties() {
        Map<String, Object> kafkaConfiguration = new HashMap<>();
        kafkaConfiguration.put(ConsumerConfig.CLIENT_ID_CONFIG, applicationId);
        kafkaConfiguration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        kafkaConfiguration.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        kafkaConfiguration.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        kafkaConfiguration.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomDeserializer.class);
        kafkaConfiguration.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);
        kafkaConfiguration.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, maxPollIntervalMs);
        return kafkaConfiguration;
    }
}
