package com.tuteggito.kafkaexample.entities;

import lombok.Data;

@Data
public class EmailData {
    private String email;
    private String subject;
    private String text;
}
